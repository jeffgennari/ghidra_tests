int num1, num2;
short num3;
int main(void) {
    num1 = 42;
    num2 = 43;
    num1 = num2 + 1;
    
    num3 = (short) num1 + num2;

    num3 -= 1;

    num3 = (num3 * num3) / 2;

    return (num1+num3) % 2;
}
